// @dart=2.9
import 'package:flutter/material.dart';
import 'package:test/screens/General.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: General(),
    );
  }
}