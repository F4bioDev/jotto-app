import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Text jottoBodyFont(string, {required Color color}) => (color != null)
    ? Text(string, style: GoogleFonts.varela(textStyle: null, color: color))
    : Text(string, style: GoogleFonts.varela(textStyle: null));

Text jottoAppBarFont(string) => Text(string,
    style:
        GoogleFonts.varela(textStyle: null, fontSize: 40, color: Colors.black));
