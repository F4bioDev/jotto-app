import 'package:flutter/material.dart';

final Color jottoColor = new Color.fromARGB(0, 0, 0, 0);
final Color jottoAppBar = new Color.fromARGB(0, 0, 0, 0);
final Color jottoBottomNavigation = new Color.fromARGB(0, 0, 0, 0);
final Color jottoBackground = new Color.fromARGB(0, 0, 0, 0);
final Color jottoShadow = new Color.fromARGB(0, 0, 0, 0);

final Color jottoColorAlternative = new Color.fromARGB(0, 0, 0, 0);
final Color jottoAppBarAlternative = new Color.fromARGB(0, 0, 0, 0);
final Color jottoBottomNavigationAlternative = new Color.fromARGB(0, 0, 0, 0);
final Color jottoBackgroundAlternative = new Color.fromARGB(0, 0, 0, 0);
final Color jottoShadowAlternative = new Color.fromARGB(0, 0, 0, 0);
