import 'package:flutter/material.dart';
import 'package:test/thermostat/thermostat.dart';

class RingPainter extends CustomPainter {
  final Color dividerColor;
  final Color glowColor;
  final double glowness;

  RingPainter({
    required this.dividerColor,
    required this.glowColor,
    required this.glowness,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = Offset.zero & size;
    final double center = size.width / 2.0;
    final Offset centerOffset = new Offset(center, center);
    final double outerRingRadius = (size.width / 2.0) - 35.0;
    final double innerRingRadius = outerRingRadius - 32.0;

    final dividerGlowPaint = Paint()
      ..color = dividerColor
      ..maskFilter = MaskFilter.blur(
        BlurStyle.outer,
        convertRadiusToSigma(4.0),
      );

    final dividerPaint = Paint()..color = dividerColor;

    final outerGlowPaint = Paint()
      ..color = glowColor
      ..maskFilter = MaskFilter.blur(
        BlurStyle.outer,
        convertRadiusToSigma(18.0 + (5.0 * glowness)),
      );

    final gradient = new RadialGradient(
      colors: <Color>[
        glowColor.withOpacity(0.5),
        glowColor.withOpacity(1.0),
      ],
      stops: [
        2.8 - (0.13 * glowness),
        1.0,
      ],
    );

    final Rect gradientRect =
        new Rect.fromCircle(center: centerOffset, radius: innerRingRadius);

    final Paint paint = new Paint()
      ..shader = gradient.createShader(gradientRect);

    canvas.saveLayer(rect, new Paint());
    //canvas.drawCircle(centerOffset, outerRingRadius, outerGlowPaint);
    canvas.drawCircle(centerOffset, innerRingRadius, paint);

    //
    canvas.translate(center, center);
    final dividerRect = Rect.fromLTWH(
        -2.0, -outerRingRadius, 4.0, outerRingRadius - innerRingRadius);
    canvas.drawRect(dividerRect, dividerPaint);
    canvas.drawRect(dividerRect, dividerGlowPaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(RingPainter oldDelegate) {
    return oldDelegate.glowColor != glowColor ||
        oldDelegate.glowness != glowness ||
        oldDelegate.dividerColor != dividerColor;
  }
}
