import 'package:flutter/material.dart';

class InfoIcon extends StatelessWidget {
  final Widget icon;
  final String text;

  const InfoIcon(this.icon, this.text);

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        icon,
        new SizedBox(width: 8.0),
        new Text(
          text,
          style: const TextStyle(
            color: const Color(0xFFA9A6AF),
            fontSize: 12.0,
          ),
        ),
        new SizedBox(width: 12.0),
      ],
    );
  }
}
