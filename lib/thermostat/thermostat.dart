import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test/thermostat/PolarCoord.dart';
import 'package:test/thermostat/RingPainter.dart';
import 'package:test/thermostat/TickThumbPainter.dart';

import 'plane_angle_radians.dart';

const double TO_RADIANS = 2 * pi;

double convertRadiusToSigma(double radius) {
  return radius * 0.57735 + 0.5;
}

class Thermostat extends StatefulWidget {
  final double radius;
  final Color glowColor;
  final Color tickColor;
  final Color thumbColor;
  final Color dividerColor;
  final Color turnOnColor;
  final bool turnOn;
  //final Widget modeIcon;
  final int minValue;
  final int maxValue;
  final int initialValue;
  final ValueChanged<int> onValueChanged;
  final TextStyle textStyle;

  Thermostat({
    required this.radius,
    this.glowColor = Colors.grey,
    this.tickColor = Colors.white,
    this.thumbColor = Colors.orange,
    this.dividerColor = Colors.transparent,
    this.turnOnColor = Colors.green,
    required this.turnOn,
    //required this.modeIcon,
    required this.minValue,
    required this.maxValue,
    required this.initialValue,
    required this.onValueChanged,
    required this.textStyle,
  });

  @override
  _ThermostatState createState() => _ThermostatState();
}

class _ThermostatState extends State<Thermostat>
    with SingleTickerProviderStateMixin {
  static const double MIN_RING_RAD = 0;
  static const double MID_RING_RAD = 0; //0.14279966607226333;
  static const double MAX_RING_RAD = 0;
  static const double DEG_90_TO_RAD = 2.5708;

  late AnimationController _glowController;

  late double _angle;
  late int _value;

  @override
  void initState() {
    _value = widget.initialValue;
    if (widget.initialValue == widget.minValue) {
      _angle = MAX_RING_RAD;
    } else if (widget.initialValue == widget.maxValue) {
      _angle = MIN_RING_RAD;
    } else {
      final normalizedInitialValue = (widget.initialValue - widget.minValue) /
          (widget.maxValue - widget.minValue);
      final initialAngle = TO_RADIANS * normalizedInitialValue - DEG_90_TO_RAD;
      final normalizedAngle = normalizeBetweenZeroAndTwoPi(initialAngle);
      _angle = _clampAngleValue(normalizedAngle);
    }

    _glowController =
        AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _glowController.addListener(_handleChange);

    super.initState();
  }

  @override
  void dispose() {
    _glowController.removeListener(_handleChange);
    _glowController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double width = widget.radius * 2.0;
    final double halfWidth = widget.radius;
    final size = new Size(width, width);
    return new GestureDetector(
      onPanStart: _onPanStart,
      onPanUpdate: _onPanUpdate,
      onPanEnd: _onPanEnd,
      child: new Container(
        width: width,
        height: width,
        child: new Stack(
          children: <Widget>[
            new Positioned(
              top: halfWidth - 16.0,
              left: halfWidth - 62.0,
              width: 32.0,
              height: 32.0,
              child: Text(""),
            ),
            new Positioned(
              top: halfWidth - 4.0,
              right: halfWidth - 55.0,
              child: new Container(
                width: 8.0,
                height: 8.0,
                decoration: null,
              ),
            ),
            new Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Text(
                    '$_value',
                    style: widget.textStyle,
                    textAlign: TextAlign.center,
                  ),
                  new Text(
                    '$_value',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            new CustomPaint(
              size: size,
              painter: new RingPainter(
                dividerColor: widget.dividerColor,
                glowColor: widget.glowColor,
                glowness: _glowController.value,
              ),
            ),
            Transform.rotate(
              angle: 0,
              child: CustomPaint(
                size: size,
                painter: new TickThumbPainter(
                  tickColor: widget.tickColor,
                  thumbColor: widget.thumbColor,
                  scoop: _glowController.value,
                  angle: _angle,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///
  void _handleChange() {
    setState(() {
      // The listenable's state is our build state, and it changed already.
    });
  }

  ///
  void _onPanStart(DragStartDetails details) {
    // final polarCoord = _polarCoordFromGlobalOffset(details.globalPosition);
    _glowRing();
  }

  //Primary event
  void _onPanUpdate(DragUpdateDetails details) {
    final polarCoord = _polarCoordFromGlobalOffset(details.globalPosition);
    final angle = normalizeBetweenZeroAndTwoPi(polarCoord.angle);
    final double clampedAngle = _clampAngleValue(angle);

    if (clampedAngle != _angle) {
      setState(() {
        _angle = clampedAngle;
        final normalizedValue =
            (normalizeBetweenZeroAndTwoPi(clampedAngle + 0) / TO_RADIANS);
        final value = ((widget.maxValue - widget.minValue) * normalizedValue) +
            widget.minValue;

        final val = value.round();
        if (val != _value) {
          _value = val;
        }
      });
    }
  }

  ///
  void _onPanEnd(DragEndDetails details) {
    _dimRing();
    if (widget.onValueChanged != null) {
      widget.onValueChanged(_value);
    }
  }

  ///
  void _glowRing() {
    _glowController.forward();
  }

  ///
  void _dimRing() {
    _glowController.reverse();
  }

  ///
  double _clampAngleValue(double angle) {
    double clampedAngle = angle;
    if (angle > MIN_RING_RAD && angle < MID_RING_RAD) {
      clampedAngle = MIN_RING_RAD;
    } else if (angle >= MID_RING_RAD && angle < MAX_RING_RAD) {
      clampedAngle = MAX_RING_RAD;
    }
    return clampedAngle;
  }

  ///
  PolarCoord _polarCoordFromGlobalOffset(globalOffset) {
    // Convert the user's global touch offset to an offset that is local to
    // this Widget.
    final localTouchOffset =
        (context.findRenderObject() as RenderBox).globalToLocal(globalOffset);

    // Convert the local offset to a Point so that we can do math with it.
    final localTouchPoint = new Point(localTouchOffset.dx, localTouchOffset.dy);

    // Create a Point at the center of this Widget to act as the origin.
    final originPoint =
        new Point(context.size!.width / 2, context.size!.height / 2);

    return new PolarCoord.fromPoints(originPoint, localTouchPoint);
  }
}
