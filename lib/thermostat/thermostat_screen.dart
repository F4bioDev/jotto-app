import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:test/thermostat/thermostat.dart';

class ThermostatScreen extends StatefulWidget {
  @override
  _ThermostatScreenState createState() => _ThermostatScreenState();
}

class _ThermostatScreenState extends State<ThermostatScreen> {
  static const textColor = Colors.black;

  late bool _turnOn;
  late int _valueThermostat;
  late BuildContext _context;
  @override
  void initState() {
    _valueThermostat = 5;
    _turnOn = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    return new Scaffold(
      body: new Container(
        padding: EdgeInsets.all(8),
        child: new SafeArea(
          child: new Column(
            children: <Widget>[
              Expanded(child: containerThermostat(), flex: 6),
              Expanded(child: containerButtons(), flex: 4),
            ],
          ),
        ),
      ),
    );
  }

  Container containerThermostat() {
    return new Container(
      child: new Center(
        child: new Thermostat(
          tickColor: Colors.orange,
          radius: 180,
          turnOn: _turnOn,
          textStyle: new TextStyle(
            color: textColor,
            fontSize: 100.0,
          ),
          minValue: 4,
          maxValue: 31,
          initialValue: 4,
          onValueChanged: valueChanged,
        ),
      ),
    );
  }

  containerButtons() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textButtons(),
          SizedBox(height: 10),
          Expanded(
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Expanded(child: statisticsButton()),
            SizedBox(width: 10),
            Expanded(child: statisticsButton()),
            SizedBox(width: 10),
            Expanded(child: statisticsButton()),
          ])),
          SizedBox(height: 10),
          Expanded(
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Expanded(child: controllerButton()),
            SizedBox(width: 7),
            Expanded(child: controllerButton()),
          ])),
        ],
      ),
    );
  }

  Container textButtons() {
    return Container(
      child: Text(
        "Quick action",
        style: TextStyle(
          color: Colors.black,
          fontSize: ResponsiveFlutter.of(context).wp(2),
        ),
      ),
    );
  }

  void valueChanged(value) {
    setState(() {
      this._valueThermostat = value;
      print(this._valueThermostat);
    });
  }

  bool almostEqual(double a, double b, double eps) {
    return (a - b).abs() < eps;
  }

  bool angleBetween(
      double angle1, double angle2, double minTolerance, double maxTolerance) {
    final diff = (angle1 - angle2).abs();
    return diff >= minTolerance && diff <= maxTolerance;
  }

  statisticsButton() {
    return OutlinedButton(
      style: ButtonStyle(
        animationDuration: Duration(seconds: 1),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
            side: BorderSide(color: Colors.red),
          ),
        ),
      ),
      onPressed: () {},
      child: Padding(
        padding: EdgeInsets.all(1),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FittedBox(
              fit: BoxFit.cover,
              child: Text(
                " OFF",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: ResponsiveFlutter.of(context).wp(2)),
              ),
            ),
            Text(
              " OFF",
              style: TextStyle(
                  color: Colors.green,
                  fontSize: ResponsiveFlutter.of(context).wp(6)),
            ),
          ],
        ),
      ),
    );
  }

  controllerButton() {
    return OutlinedButton(
      style: ButtonStyle(
        animationDuration: Duration(seconds: 1),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: Colors.red),
          ),
        ),
      ),
      onPressed: () {},
      child: Padding(
        padding: EdgeInsets.only(top: 15, bottom: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Fan",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: ResponsiveFlutter.of(context).wp(2))),
            SizedBox(height: 1),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.ac_unit_outlined),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    " OFF",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ResponsiveFlutter.of(context).wp(6)),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
