import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test/screens/BottomNavigationBar/JottoBottomNavigationBar.dart';
import 'package:test/screens/appBar/JottoAppBar.dart';
import 'package:test/thermostat/thermostat_screen.dart';

class General extends StatelessWidget {
  const General({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: myAppBar(),
        body: ThermostatScreen(),
        bottomNavigationBar: jottoNavigationBar());
  }
}
