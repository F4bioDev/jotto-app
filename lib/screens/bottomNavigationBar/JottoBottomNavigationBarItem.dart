import 'package:flutter/material.dart';
import 'package:test/constants/JottoFonts.dart';

jottoBottomNavigationItem(icon, text, event) {
  return BottomNavigationBarItem(
    icon: new Icon(
      icon,
      color: Colors.black,
    ),
    // ignore: deprecated_member_use
    title: jottoBodyFont(text, color: Colors.black),
  );
}
