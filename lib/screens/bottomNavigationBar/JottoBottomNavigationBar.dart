import 'package:flutter/material.dart';
import 'package:test/screens/BottomNavigationBar/JottoBottomNavigationBarItem.dart';

jottoNavigationBar() {
  return BottomNavigationBar(
    elevation: 35,
    iconSize: 35,
    backgroundColor: Colors.white38,
    currentIndex: 0, // this will be set when a new tab is tapped
    items: [
      jottoBottomNavigationItem(Icons.ac_unit, "text", null),
      jottoBottomNavigationItem(Icons.ac_unit, "text", null),
      jottoBottomNavigationItem(Icons.ac_unit, "text", null),
    ],
  );
}
